package main;

import java.sql.SQLException;





public class Customer extends Person {

	public double balance;
	public String customerState;
	
	
	public Customer() {
		super();
	}
	
	public Customer(int id, String firstName, String lastName, String address, String phone) {
		super(id, firstName, lastName, address, phone);
	}
	
	public Customer(int id, String firstName, String lastName, String address,
			String phone, String customerState, double balance) {
		super(id, firstName, lastName, address, phone);
		this.balance = balance;
		this.customerState = customerState;
	}
	

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public String getCustomerState() {
		return customerState;
	}

	public void setCustomerState(String customerState) {
		this.customerState = customerState;
	}

	public boolean userRegister(String firstName,String lastName,String address ,String phone) throws SQLException, MyException
	{
		DB db = new DB();// create the DB object
	    db.initialise();// initiate connection to the DB
	
	    if(db.DBRegister(firstName, lastName, address, phone))
		{
			return true;
		}
		
		else
		System.out.println("You give wrong register information please enter again !");
		 return false ;
		 
	}
	

   
}
