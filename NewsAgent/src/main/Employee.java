package main;


public class Employee extends Person {

	
	
	public Employee() {
		super();
	}
	
	public Employee(int id, String firstName, String lastName, String address, String phone) {
		super(id, firstName, lastName, address, phone);
	}
}