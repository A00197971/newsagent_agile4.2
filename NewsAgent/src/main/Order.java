package main;
public class Order {

	private int id;
	private String details;
	private int price;
	
	public Order() { }
	
	public Order(int id, String details, int price) {
		this.id = id;
		this.details = details;
		this.price = price;
	}
	
	public int getID() {
		return id;
	}
	
	public void setID(int id) {
		this.id = id;
	}
	
	public String getDetails() {
		return details;
	}
	
	public void setDetails(String details) {
		this.details = details;
	}
	
	public int getPrice() {
		return price;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
}
