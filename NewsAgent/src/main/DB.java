package main;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;







public class DB {

	private static DB db = null;
	private Connection connection;
	private Statement statement;
    private PreparedStatement psmt;
	public DB() {
		initialise();
	}

	public static synchronized DB getInstance() {
		if (db == null) {
			db = new DB();
			return db;
		} else {
			return db;
		}
	}

	public boolean initialise() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3307/newsagent";
			connection = DriverManager.getConnection(url, "root", "admin");
			statement = connection.createStatement();
			return true;
		} catch (Exception e) {
			System.err.println(e);
			return false;
		}
	}

	public ResultSet executeQuery(String query) {
		try {
			return statement.executeQuery(query);
		} catch (Exception e) {
			System.err.println(e);
		}
		return null;
	}

	public void executeUpdate(String query) {
		try {
			statement.executeUpdate(query);
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	/**
	 * @param firstname
	 * @param lastname
	 * @param address
	 * @param phone
	 * @return
	 */
	public boolean DBRegister(String firstname, String lastname, String address, String phone)throws SQLException,MyException {
		// default each new customer's balance is 0.
		double balance=0.00;
		// if each attribute meets the requirements above i.e. all strings are
		// not null and less than 15 chars,
		// then we will run try block to try to insert the customer details into the db
		if ((firstname.length() > 15) || (lastname.length()>15) || (phone.length()> 15) || (address.length() > 15)) {
	    // Execute the SQL statement "Select" to get the firstname and lastname of cusomer
			MyException e= new MyException("The length of paramaters you input is greater than 15 !");
  	        System.out.println(e.getMessage());
		 return false;
		}else if ((firstname == " ") || (lastname == "") || (address == "") || (phone == "")){
			MyException e= new MyException("paramaters you input is null !");
  	        System.out.println(e.getMessage());
			return false;
		}else{
			
		String query= "Select firstname,lastname from customer where firstname = '" + firstname + "' and lastname = '" + lastname + "';";			
		
		System.out.println(query);
		ResultSet rs = statement.executeQuery(query);
		
		// if the customer doesn't exist in database ,then do the try block(register the customer for the customer)
		if(!rs.next()){
		// (insert) Execute the SQL statement "insert"
		
						String updateTemp = "INSERT INTO customer(id, firstname,lastname,address,phone,balance) VALUES(" + null + ",'"
								+ firstname + "','" + lastname + "','" + address + "','"
								+ phone + "','"+balance+"');";
		                System.out.println(updateTemp);
						statement.executeUpdate(updateTemp);
						 return true;
						 
		}
		else{
			System.err.println("You have already an account in database !");
		
		return false;}
		}
		
	}
	

	
	
	
}