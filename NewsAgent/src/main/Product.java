package main;

public class Product {

	private int id;
	private String name;
	private int price;
	
	public Product() { }
	
	public Product(int id, String name, int price) {
		this.id = id;
		this.name = name;
		this.price = price;
	}
	
	public Product(String name) {
		super();
		this.name = name;
	}

	
	public Product(int pid) {
		id=pid;
		
	}

	public int getID() {
		return id;
	}
	
	public void setID(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getPrice() {
		return price;
	}
	
	public void setprice(int price) {
		this.price = price;
	}
}
