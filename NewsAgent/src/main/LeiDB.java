
package main;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import main.Customer;
import main.DB;
import main.Product;

/**
 * @author Lei Cao
 *
 * 2016��3��23��
   ����10:17:18
   2016
   This class contains methods for checking the customer information if they are fully correct 
   when newsagent print out the order per day, which can help delivery guy is on the right way
   and make sure work smoothly and ok.
   Methods including:
   1.Verify an customer if he/she is in database
      1.1. DBCheckExist(String firstname, String lastname) 
              check a certain customer on order if he/she exist ! and get detail of that customer 
      1.2 checkListofCustomer(list<Customer) 
              check all of the customer on order if they all exist ,if they all exist ,then print
              them out ,otherwise point out who doesn't exist
   2. Verify customer if they are available to receive the publication
      2.1. getCustomerState(int cid)
   3.verify an certain customer if his /her amount is correct responding to customer
      3.1. getAmount(int id)
   4.verify an certain customer if his/her balance is lower than 0
 	  4.1.  getBalance(int id)
   5.verify an certain customer if his/her address is correct
      5.1.  getAddress(int cid)
   6.verify an certain customer whose ID and name if they are matching.
      6.1.  getCustomerByID(int id)
   7.verify the publication if it is corresponding to the one required.  
      7.1  getPublication(int cid) 
           Note :This method return name of publication ,how does it work?
                 so first get the publication id by customer id in order table,
                 then go to product table to get the publication name by product id.
                 This is why I create a ProductDAO(deal with the database).
   Note: All of these are just one user story(New 13).
 */
public  class LeiDB {

	private DB db;
	private static LeiDB leiDB = null;
	public LeiDB(){
		db = DB.getInstance();
		}
	
	public synchronized static LeiDB getInstance() {
	    if (leiDB == null) {
	    	leiDB = new LeiDB();
	        return leiDB;
	    } else {
	        return leiDB;
	    }
	}
	    // 1.Verify an customer if he/she is in database, if it is there , then print out the details of customer.
		// if not return false to show the customer does not exist.
		public boolean DBCheckExist(String firstname, String lastname) throws SQLException,MyException
		{	
			// Execute the SQL statement "Select" to get all information about customer 
			String query= "Select * from customer where firstname= '"+firstname+"' and lastname='"+lastname+"' ;";	 
			System.out.println(query);
			ResultSet rs = db.executeQuery(query);
			if(rs.next()){
				 MyException e= new MyException("There is the customer! The details is below !");
	        	  System.out.println(e.getMessage()); 
				System.out.println("FirstName\tLastname\tAddress\t\tphone\t\tBalance");
			System.out.println(rs.getString(2)+"\t\t" +rs.getString(3)+"\t\t" +rs.getString(4)+"\t" +rs.getString(5)+"\t"+rs.getDouble(7));
			 return true;
			}
	          else {
	        	  
	        	  MyException e= new MyException("There is no customers! so no detail information !");
	        	  System.out.println(e.getMessage()); 
	        	  return false; }
				
			
			}
		//1.1 
		public boolean checkListofCustomer(List<Customer> lc)throws SQLException,MyException{
			ResultSet rs ;
			int count=0;
			for(int i=0;i<lc.size();i++){
				String firstName=lc.get(i).getFirstName();
				String lastName=lc.get(i).getLastName();
				String query= "Select * from customer where firstname= '"+firstName+"' and lastname='"+lastName+"' ;";
				 rs = db.executeQuery(query);
				 if(rs.next()){
					 count++;
					 MyException e= new MyException(firstName+" "+lastName+"  exist, The details is below !");
		        	  System.out.println(e.getMessage()); 
					
				}
		          else {
		        	  
		        	  MyException e= new MyException(firstName+" "+lastName+"  doesn't exist,! so no detail information !");
		        	  System.out.println(e.getMessage()); 
		        	   }
			}
			
			if(count==lc.size()){
				System.out.println("FirstName\tLastname\tAddress\t\tphone\t\tBalance\tState");
				for(int i=0;i<lc.size();i++){
					String firstName=lc.get(i).getFirstName();
					String lastName=lc.get(i).getLastName();
					String address=lc.get(i).getAddress();
					String phone=lc.get(i).getPhone();
					Double balance=lc.get(i).getBalance();
					String state=lc.get(i).getCustomerState();
					
					System.out.println(firstName+"\t\t" +lastName+"\t\t" +address+"\t" +phone+"\t"+balance+"\t"+state);
					
				}
				
				return true;
				
			}else{
				 MyException e= new MyException("some customers do not exist");
	        	  System.out.println(e.getMessage()); 
				return false;
			}	
			
			
		}
		 //2. verify a certain customer if he/she is available to recieve the publication by checking his/her state currently.
		   public String getCustomerState(int cid)throws SQLException,MyException{
			   ResultSet resultSet = db.executeQuery("SELECT CustomerStates FROM customer WHERE ID = " + cid + ";");
			   
			   String customerState=null;
			   if (resultSet.next()) {
				   customerState=resultSet.getString("CustomerStates");
				   
				   if(customerState.equals("ACTIVE")){
					   System.out.println(customerState);
					   
					   MyException e= new MyException(" The customer with "+cid+" is available !");
			   	        System.out.println(e.getMessage());
					   }
				    else {//customerState=="UNACTIVE"
				    	System.out.println(customerState);
				    	MyException e= new MyException(" The customer  with "+cid+" is not available !");
			   	        System.out.println(e.getMessage());
				    	
				    	}
				   return customerState;
			   }else{
				   MyException e= new MyException(" The customer  with "+cid+" doesn't exist !");
	   	        System.out.println(e.getMessage());
	   	        return "customer doen't exist";}
			  
			  
		   }
		 //3. Method to get amount of publication which customers required.
			public LeiOrder getAmount(int id)throws SQLException, MyException {
				
				 ResultSet resultSet = db.executeQuery("SELECT CID,Amount FROM ORDERs WHERE CID = " + id + ";");
				 LeiOrder order=null;
				 if (resultSet.next()){
					 order=new LeiOrder(resultSet.getInt(1),resultSet.getInt(2)) ;
					 
			   	        System.out.println(" The customer with id="+id+" which you are looking for have been founded !");
				
			   	     return order;}
				 else{ 
		         	MyException e= new MyException(" The customer with id="+id+" which you find does not exist!");
		   	        System.out.println(e.getMessage());
		   	        return null;
		   	        }
				
				
				
			}
			//4.Method to get balance of a certain customer ,which verify if balance of the customer is less than 0
			// if it is ,then the customer should not show up on order list.
		   public double getBalance(int id) throws SQLException, MyException{
			     
			    // Get the id of publication by customer id from table orders.
			     ResultSet resultSet = db.executeQuery("SELECT Balance FROM CUSTOMER WHERE ID = " + id + ";");
			     double balance=0.00;
			     if (resultSet.next()){
			    	 balance=resultSet.getDouble("Balance");
			    	 System.out.println(" The customer with id="+id+" which you are looking for have been founded !");
			    	 return balance;
			     }
			     else{ 
			         	MyException e= new MyException(" The customer does not exist !");
			   	        System.out.println(e.getMessage());
			   	        return -1;
			   	        }
			  
		   }
		// 5.verify an certain customer if his/her address is correct
			public String getAddress(int cid) throws SQLException, MyException{
				ResultSet resultSet = db.executeQuery("SELECT ADDRESS FROM CUSTOMER WHERE ID = " + cid + ";");
				String address=null;
				 if (resultSet.next()){
					
					address=resultSet.getString("Address");
					 System.out.println(" The customer with id="+cid+" which you are looking for have been founded !");
					 return address;
				 } else{ 
			         	MyException e= new MyException(" The customer did not order any publication!");
			   	        System.out.println(e.getMessage());
			   	     return null;
			   	        }
			
				
				
			}
		   
	
	// 6.verify an certain customer whose ID and name if they are matching.
	// method to get information of a certain customer by id
	public Customer getCustomerByID(int id)throws SQLException, MyException {
		
		
        ResultSet resultSet = db.executeQuery("SELECT * FROM customer WHERE id = " + id + ";");
        Customer customer = null;
         if (resultSet.next()) {
                customer = new Customer(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4),resultSet.getString(5),resultSet.getString(6),resultSet.getDouble(7));
                System.out.println(" The customer with id="+id+" which you are looking for have been founded !");  
         }
            else{ 
            	MyException e= new MyException(" The customer which you find does not exist!");
      	        System.out.println(e.getMessage());
      	        }
            
        
        return customer;

	}
	
	//7.Method to get the name of publication by customer id
	public String getPublication(int cid)throws SQLException, MyException{
		// Get the id of publication by customer id from table orders.
		 ResultSet resultSet = db.executeQuery("SELECT PID FROM ORDERs WHERE CID = " + cid + ";");
		 int pid=0;
		 if (resultSet.next()){
			pid=resultSet.getInt("PID");
			 System.out.println(" The customer with id="+cid+" which you are looking for have been founded !");
		 } else{ 
	         	MyException e= new MyException(" The customer did not order any publication!");
	   	        System.out.println(e.getMessage());
	   	        return null;
	   	        }
		String publication=null;
		Product product=new Product();
		//Get the publication name by publication id form table product.
		publication =ProductDAO.instance.getProductName(pid);
		
		return publication;
		
	}
	
		
	
	
	//Get a list of order for a certain customer by customer id
   public LeiOrder getOrderById(int cid) throws SQLException,MyException{
	   ResultSet resultSet = db.executeQuery("SELECT * FROM ORDERS WHERE ID = " + cid + ";");
	   LeiOrder order = null;
       if (resultSet.next()) {
    	   order = new LeiOrder(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), resultSet.getDate(4),resultSet.getInt(5),resultSet.getDouble(6));
    	   System.out.println(" The customer with id="+cid+" which you are looking for have been founded !");  
       }
          else{ 
          	MyException e= new MyException(" The customer which you find does not exist!");
    	        System.out.println(e.getMessage());
    	        }
          
      
      return order;
	   
   }
  
   
}



