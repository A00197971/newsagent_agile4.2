package main;

import java.util.Date;


public class LeiOrder {

	private int id;
	private String details;
	private int price;
	private int cid;
	private int amount;
	private Product product;
	private Date date;
	private Double TotalPrice;
	
	public LeiOrder() { }
	
	public LeiOrder(int id, String details, int price) {
		this.id = id;
		this.details = details;
		this.price = price;
	}
	public LeiOrder(int cid) {
		super();
		this.cid = cid;
		
	}
	
	public LeiOrder(int cid, String title) {
		super();
		this.cid = cid;
		product=new Product(title);
	}

	public LeiOrder(int cid, int amount) {
		super();
		this.cid = cid;
		this.amount = amount;
	}

	
	public LeiOrder(int id, int cid, int pid , Date date,int amount, Double totalPrice) {
		super();
		this.id = id;
		this.cid = cid;
		this.amount = amount;
		this.date = date;
		TotalPrice = totalPrice;
		product=new Product(pid);
	}

	public int getCid() {
		return cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getID() {
		return id;
	}
	
	public void setID(int id) {
		this.id = id;
	}
	
	public String getDetails() {
		return details;
	}
	
	public void setDetails(String details) {
		this.details = details;
	}
	
	public int getPrice() {
		return price;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
}
