
package main;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import main.DB;

/**
 * @author Caolei
 *
 */
public enum ProductDAO {
	 instance;

	  public Connection connection = null;
		private void closeCon(){
			try{
				if(connection!=null){
					connection.close();
					connection=null;
				}
			}catch(Exception e){
				
			}
		}
		
	// private DB db;
		
         // Method to get the product name by product ID 
		public String getProductName(int productId) {
			Connection connection = Utils.getConnection();
			
			String ProductName=null;
			 try {
				
				 
		         String sql= "SELECT TITLE FROM PRODUCT WHERE ID = ?";
		         PreparedStatement psmt = connection.prepareStatement(sql);
		         psmt.setInt(1, productId);

		         ResultSet rs = psmt.executeQuery();
				if (rs.next()){
					ProductName = rs.getString("Title");
				}
		         
		          
		         
		      }  catch (SQLException e) {
		         e.printStackTrace();
		      }
			return ProductName;
		}

		

		

		/*public double getProductPrice(int productId) {
			
			Connection connection = Utils.getConnection();
			double ProductPrice=0;
			try {
				 String sql = "SELECT * FROM Product WHERE id = ?";
		         PreparedStatement psmt = connection.prepareStatement(sql);
		         psmt.setInt(1, productId);

		         ResultSet rs = psmt.executeQuery();
		         rs.next();
		         ProductPrice = rs.getDouble("Price");
			} catch (SQLException e) {
			e.printStackTrace();
		}
			return ProductPrice;
		}*/

	

}
