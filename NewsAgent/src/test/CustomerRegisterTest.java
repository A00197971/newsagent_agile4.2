
package test;

import java.sql.SQLException;

import main.MyException;
import main.Customer;
import junit.framework.TestCase;

/**
 * @author Caolei
 *
 */
public class CustomerRegisterTest extends TestCase {

	// Test no.:1
	// Test Objective: Test user Register (correct user name and password //
	// entered)
	// Input(s): String firstname = Mike,String lastname =koc,String address=5
	// willow view,
	// String phone="0894600264"
	// Expected Outputs: Register failed (boolean false,product code has not
	// been written yet)
	public void testRegisterValid001() throws SQLException, MyException {
		// Create customer object
		Customer customer = new Customer();
		// Test the method
		assertTrue(customer.userRegister("Mike", "koc", "5 willow view",
				"0894600264"));
	}

	// Test no.: 2
	// Test Objective: Test user Register
	// Input(s): String firstname = Mike,String lastname =koc,String address=5
	// willow view, String phone="0894600264"
	// Expected Outputs: Register sucessfully (boolean true, each paramater is
	// ok)
	public void testRegistervalid002() throws SQLException, MyException {

		// Create customer object
		Customer customer = new Customer();
		// Test the method
		assertFalse(customer.userRegister("Mike", "koc", "5 willow",
				"089460026"));
	}

	// Test no.: 3
	// Test Objective: Test user Register (
	// Input(s): String firstname =null,String lastname =null,String address=5
	// willow view, String phone="0894600264"
	// Expected Outputs: Register failed (boolean false, any paramater is not
	// allowed to be null)
	public void testRegisterInvalid003() throws SQLException, MyException {

		// Create customer object
		Customer customer = new Customer();
		// Test the method
		assertFalse(customer.userRegister("", " ", "5 willow", "0894600264"));
	}

	// Test no.: 4
	// Test Objective: Test user Register (
	// Input(s): String firstname =fang,String lastname =YE,String address=5
	// willow place Athlone Co.Westmeath
	// String phone="08744321"
	// Expected Outputs: Register failed (boolean false, address.length >15)

	public void testRegisterInvalid004() throws SQLException, MyException {

		// Create customer object
		Customer customer = new Customer();
		// Test the method
		assertFalse(customer.userRegister("fang", "YE ",
				"5 willow place Athlone Co.Westmeath", "08744321"));
	}

}
