
package test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import main.LeiDB;
import main.LeiOrder;
import main.MyException;
import main.Customer;
import junit.framework.TestCase;
/**
 * @author Lei Cao
 *
 * 2016��4��10��
   ����11:16:54
   2016
   Test 7 Methods :
    1.DBCheckExist(String firstname, String lastname)
    2.getCustomerState(int cid)
    3.getAmount(int id)
    4.getBalance(int id)
    5.getAddress(int cid)
    6.getCustomerByID(int id)
    7.getPublication(int cid)
   
   
 */
public class DBTest extends TestCase {
	
	    private LeiDB leiDB = null;
	  
	    // Test No: 001
	    // Objective:  Test database connection
	    // Inputs: leiDB
	    // Expected Output: pass
	   // Note: Test LeiDB object leiDB if it is null, if it is ,database connect failed, otherwise successfully !
	    public void testGetInstance001() {
	        assertNull(leiDB);
	    }
       /* 
        * 1.Test method DBCheckExist(String firstname, String lastname)
        */
	    //Test No: 002
		// Test Objective: Test database if the customer exists
		// Input(s): 	
		// Expected Outputs: boolean true
		// Note:  The product code of DBgetCustomerInfo is ready now.
		public void testDBCheckExistValid001() throws SQLException, MyException {
			//Create db object
			leiDB = LeiDB.getInstance();
			// Test the method
			
			assertTrue(leiDB.DBCheckExist("Hua","Wu"));
		}
		
		// Test No: 003
		// Test Objective: Test database if the customer exists
		// Input(s): 	
		// Expected Outputs: boolean false
		// Note:  The customer does not exist in database.
		public void testDBCheckExistInValid002() throws SQLException, MyException {
			//Create db object
			leiDB = LeiDB.getInstance();
			// Test the method
			assertFalse(leiDB.DBCheckExist("Smith","J"));
		}
		
		/*
		 * 1.1 Test  method checkListofCustomer
		 */
		      //Test No: 004
				// Test Objective: Test database if all customers exist
				// Input(s): Customer 1, Customer 2,Customer3	
				// Expected Outputs: boolean false;
				// Note:  The last of customer doen't exist.
		public void testcheckListofCustomerInvalid001() throws SQLException, MyException{
			leiDB = LeiDB.getInstance();
			List<Customer> lc=new ArrayList();
			 Customer c1 = new Customer(1, "Lei", "Cao", "4  Willow place", "0894600264","ACTIVE",26);
			 Customer c2 = new Customer(2,"Hua","Wu","Jiu Jiang","15879211276","ACTIVE",0);
			 Customer c3 = new Customer(4,"Lee","Li","5 willow place","13916245593","ACTIVE",0);
			 lc.add(c1);
			 lc.add(c2);
			 lc.add(c3);
			 assertFalse(leiDB.checkListofCustomer(lc));
		}
		//Test No: 005
		// Test Objective: Test database if all customers exist
		// Input(s): Customer 1, Customer 2,Customer3	
		// Expected Outputs: boolean true;
		// Note:  All customer  exist.
		public void testcheckListofCustomerValid002() throws SQLException, MyException{
			leiDB = LeiDB.getInstance();
			List<Customer> lc=new ArrayList();
			 Customer c1 = new Customer(1, "Lei", "Cao", "4  Willow place", "0894600264","ACTIVE",26);
			 Customer c2 = new Customer(2,"Hua","Wu","Jiu Jiang","15879211276","ACTIVE",0);
			 Customer c3 = new Customer(3,"Mike","koc","5 willow place","13916245593","ACTIVE",0);
			 lc.add(c1);
			 lc.add(c2);
			 lc.add(c3);
			 assertTrue(leiDB.checkListofCustomer(lc));
		}
		/* 
	     * 2.Test method getCustomerState(int cid)
	     */
		 // Test No: 006
	    // Objective: Test GetCustomerState(int id) method  .
	    // Objective: verify the customer if they are available to receive the ordered publication. 
	    // Objective: if the customer state is inactive, the customer should not appear in order list !
	    // Inputs: id = 1
	    // Expected Output: successfully
	    // Note : The state of customer is active and on the order list.
	    public void testGetCustomerStateValid001() throws SQLException, MyException {
	    	
	    	leiDB = LeiDB.getInstance();
	    	
	    	 String cutomerState=leiDB.getCustomerState(1);
	    	
	    	 LeiOrder order=leiDB.getOrderById(1);
	    	 boolean flag1=false;
	    	 boolean flag2=false;
	    	 boolean flag3=false;
	    	 flag1=cutomerState.equalsIgnoreCase("ACTIVE");
	    	flag2=order!= null;
	    	flag3=flag1 && flag2;
		        // if the state of customer is inactive and his/her name is on the orders
		        // because of some reasons (including the customer go to holiday or his/her balance is 0) get his/her account to be inactive
		        // so even the customer order publication beyond the date or his/her balance, the order should be cancelled.
	    	// assertTrue(cutomerState.equalsIgnoreCase("UNACTIVE") && order!= null);
	    	 assertTrue(flag3);
	    	
	    }
	    
	    // Test No: 007
	    // Objective: Test GetCustomerState(int id) method return .
	    // Objective: verify the customer if they are available to receive the ordered publication. 
	    // Objective: if the customer state is inactive, the customer should not appear in order list !
	    // Inputs: id = 3
	    // Expected Output: failed
	    // Note : The state of customer is inactive but he is on the order list.
	    public void testGetCustomerStateInvalid002() throws SQLException, MyException {
	    	
	    	leiDB = LeiDB.getInstance();
	          String cutomerState=leiDB.getCustomerState(3);
	        
	         LeiOrder order=leiDB.getOrderById(3);
	        
	        // if the state of customer is inactive and his/her name is on the orders
	        // because of some reasons (including the customer go to holiday or his/her balance is 0) get his/her account to be inactive
	        // so even the customer order publication beyond the date or his/her balance, the order should be cancelled.
	         assertFalse(cutomerState.equalsIgnoreCase("ACTIVE") && order!= null);
	    	
	    	
	    }
	  
	    // Test No: 008
	    // Objective: Test GetCustomerState(int id) method return .
	    // Objective: verify the customer if they are available to receive the ordered publication. 
	    // Objective: if the customer state is inactive, the customer should not appear in order list !
	    // Inputs: id = 9
	    // Expected Output: failed
	    // Note : The customer with id=9 doesn't exist.
	    public void testGetCustomerStateInvalid004() throws SQLException, MyException {
	    	
	    	leiDB = LeiDB.getInstance();
	          String cutomerState=leiDB.getCustomerState(9);
	          System.out.println(cutomerState);
	         LeiOrder order=leiDB.getOrderById(9);
	        
	        // if the state of customer is inactive and his/her name is on the orders
	        // because of some reasons (including the customer go to holiday or his/her balance is 0) get his/her account to be inactive
	        // so even the customer order publication beyond the date or his/her balance, the order should be cancelled.
	         assertFalse(cutomerState.equalsIgnoreCase("ACTIVE") && order!= null);
	    	
	    	
	    }
	    
	    /* 
	     * 3.Test method getAmount(int id)
	     */
	    // Test No: 009
	    // Objective: Test GetAmount(int id) method returns correct amounts of the publication which customer required from database.
	    // Objective: verify the customer data if they are correct.
	    // Inputs: id = 1
	    // Expected Output: successfully 
	    // Note : The product is ready now and the data is correct
	    public void testGetAmountValid001() throws SQLException, MyException {
	    	
	    	leiDB = LeiDB.getInstance();
	        LeiOrder order = leiDB.getAmount(1);
	        LeiOrder order2 = new LeiOrder(1, 1);
	      
	        assertEquals(order2.getAmount(), order.getAmount());
	        

	    }
	    // Test No: 010
	    // Objective: Test GetAmount(int id) method returns correct amounts of the publication which customer required from database.
	    // Objective: verify the customer data if they are correct.
	    // Inputs: id = 1
	    // Expected Output: failed
	    // Note : The product is ready now ,but the amount =2 in order is not mapping with database.
	    public void testGetAmountInvalid002() throws SQLException, MyException {
	    	
	    	leiDB = LeiDB.getInstance();
	        LeiOrder order = leiDB.getAmount(1);
	        LeiOrder order2 = new LeiOrder(1, 2);
	        assertFalse(order2.getAmount()==order.getAmount());
	       

	    }
	 // Test No: 011
	    // Objective: Test GetAmount(int id) method returns correct amounts of the publication which customer required from database.
	    // Objective: verify the customer data if they are correct.
	    // Inputs: id = 9
	    // Expected Output: failed
	    // Note : The customer with id=9 doesn't exist.
	    public void testGetAmountInvalid003() throws SQLException, MyException {
	    	
	    	leiDB = LeiDB.getInstance();
	        LeiOrder order = leiDB.getAmount(9);
	        
	        assertFalse(order!=null);
	       

	    }
	    
	    /* 
	     * 4.Test method getBalance(int id)
	     */
	    // Test No: 012
	    // Objective: Test getGetBalance(int id) method return a double type of balance from database.
	    // Objective: verify balance of cutomer if is equal to zero, if it is true, 
	    // Objective: the customer should not appear in order list!.
	    // Inputs: cid = 1 
	    // Expected Output: successfully 
	    // Note : The product code is ready.The balance of the customer is 26 >0
	    public void testGetBalanceValid001() throws SQLException, MyException {
	    	
	    	leiDB = LeiDB.getInstance();
	        double balance = leiDB.getBalance(1);
	        LeiOrder order=leiDB.getOrderById(1);
	        
	    	assertTrue(balance>0 && order!= null);
	    }
	 // Test No: 013
	    // Objective: Test getBalance(int id) method return a double type of balance from database.
	    // Objective: verify balance of cutomer if is equal to zero, 
	    // Objective: if it is true,  the customer should not appear in order list!.
	    // Inputs: cid = 2
	    // Expected Output: failed. 
	    // Note : The product code is ready.The customer with id =2 have no balance in account
	    public void testGetBalanceInvalid002() throws SQLException, MyException {
	    	
	    	leiDB = LeiDB.getInstance();
	        double balance = leiDB.getBalance(2);
	        LeiOrder order=leiDB.getOrderById(2);
	       
	    	assertFalse(balance>0 && order!= null);
	    }
	 // Test No: 014
	    // Objective: Test getBalance(int id) method return a double type of balance from database.
	    // Objective: verify balance of cutomer if is equal to zero, 
	    // Objective: if it is true,  the customer should not appear in order list!.
	    // Inputs: cid = 9
	    // Expected Output: failed. 
	    // Note :  The customer with id=9 doesn't exist.
	    public void testGetBalanceInvalid003() throws SQLException, MyException {
	    	
	    	leiDB = LeiDB.getInstance();
	        double balance = leiDB.getBalance(9);
	        
	       
	    	assertFalse(balance>0);
	    }
	    /* 
	     * 5.Test method  getAddress(int cid)
	     */
	    // Test No: 015
	    // Objective: Test getAddress(int cid) to check the address of a certain customer correct or not
	    //Inputs��cid=1;
	    //Expected Output:successfully
	    //Note:The address of a customer provided on list is matching to  database record
         public void testGetAddressvalid001() throws SQLException, MyException {
	    	
	    	leiDB = LeiDB.getInstance();
	        String  address = leiDB.getAddress(1);
	        Customer customer2 = new Customer(1, "Lei", "Cao", "4  Willow place", "0894600264","ACTIVE",26);
	        assertEquals(customer2.getAddress(),address );
	    	
	    }
	    // Test No:016
	   
	    // Objective: Test getAddress(int cid) to check the address of a certain customer correct or not
	    //Inputs��cid=1;
	    //Expected Output:failed
	    //Note:The address of a customer provided on list doesn't match  database record
         public void testGetAddressInvalid001() throws SQLException, MyException {
 	    	
 	    	leiDB = LeiDB.getInstance();
 	        String  address = leiDB.getAddress(1);
 	        Customer customer2 = new Customer(1, "Lei", "Cao", "5  Willow place", "0894600264","ACTIVE",26);
 	        assertFalse(customer2.getAddress()==address );
 	    	
 	    }
         // Test No:017
  	   
 	    // Objective: Test getAddress(int cid) to check the address of a certain customer correct or not
 	    //Inputs��cid=9;
 	    //Expected Output:failed
 	    //Note:The address of a customer provided on list doesn't match  database record
          public void testGetAddressInvalid002() throws SQLException, MyException {
  	    	
  	    	leiDB = LeiDB.getInstance();
  	        String  address = leiDB.getAddress(9);
  	        
  	        assertFalse(address!=null );
  	    	
  	    }
         
	    /* 
	     * 6.Test method  getCustomerByID(int id)
	     * 
	     */    
	 // Test No: 018
	    // Objective: Test getCustomerByID(int id) method returns correct values from database.
	    // Objective: verify the customer data if they are correct.
	    // Inputs: id = 1
	    // Expected Output: same value,pass
	    public void testGetCustomerByIDValid001() throws SQLException, MyException {
	    	leiDB = LeiDB.getInstance();
	        Customer customer = leiDB.getCustomerByID(1);
	        Customer customer2 = new Customer(1, "Lei", "Cao", "4  Willow place", "0894600264","ACTIVE",26);
	        //Customer customer2 = new Customer(1, "Joe", "Mullins", "Athlone1", "123456789");
	        assertEquals(customer2.getID(), customer.getID());
	        assertEquals(customer2.getFirstName(), customer.getFirstName());
	        assertEquals(customer2.getLastName(), customer.getLastName());
	        assertEquals(customer2.getAddress(), customer.getAddress());
	        assertEquals(customer2.getPhone(), customer.getPhone());
	        assertEquals(customer2.getCustomerState(), customer.getCustomerState());
	        assertEquals(customer2.getBalance(), customer.getBalance());
	        
	       
	    }
	 // Test No: 019
	    // Objective: Test getCustomerByID(int id) method returns correct values from database.
	    // Objective: verify the customer data if they are correct.
	    // Inputs: id = 1, address=6  Willow place
	    // Expected Output:failed, the address is not mapping(the correct address=4  Willow place).
	    public void testGetCustomerByIDInvalid002() throws SQLException, MyException {
	    	leiDB = LeiDB.getInstance();
	        Customer customer = leiDB.getCustomerByID(1);
	        Customer customer2 = new Customer(1, "Lei", "Cao", "6  Willow place", "0894600264","ACTIVE",26);
	        //Customer customer2 = new Customer(1, "Joe", "Mullins", "Athlone1", "123456789");
	        assertTrue(customer2.getID()==customer.getID());
	        assertFalse(customer2.getFirstName()==customer.getFirstName());
	        assertFalse(customer2.getLastName()==customer.getLastName());
	        assertFalse(customer2.getAddress()==customer.getAddress());
	        assertFalse(customer2.getPhone()==customer.getPhone());
	        assertFalse(customer2.getCustomerState()==customer.getCustomerState());
	        assertFalse(customer2.getBalance()!=customer.getBalance());
	        
	       
	    }
	    /* 
	     * 7.Test method   getPublication(int cid)
	     * 
	     */ 
	 // Test No: 020
	    // Objective:  Test GetPublication(int Cid) method returns correct  publication name which customer required from database.
	    // Objective: verify publication name in order if is required by customer. Make sure the customer with id=1 order publication 
	    // named IrishTime on orders for each day.
	    // Inputs: cid = 1 Name="IrishTime"
	    // Expected Output: successfully 
	    // Note : The product is ready now.
	    public void testGetPublicationValid001() throws SQLException, MyException {
	    	
	    	leiDB = LeiDB.getInstance();
	        String pub = leiDB.getPublication(1);
	        assertEquals("IrishTime",pub);
	        

	    }
	    // Test No: 021
	    // Objective:  Test GetPublication(int Cid) method returns correct  publication name which customer required from database.
	    // Objective: verify publication name in order if is required by customer. Make sure the customer with id=1 order publication 
	    // named IrishTime on orders for each day.
	    // Inputs: cid = 1 Name="Travel"
	    // Expected Output: failed 
	    // Note : the customer with id=1 order IrishTime instead of Travel.
	    public void testGetPublicationInvalid002() throws SQLException, MyException {
	    	
	    	leiDB = LeiDB.getInstance();
	        String pub = leiDB.getPublication(1);
	        assertFalse(pub.equalsIgnoreCase("Travel"));
	        

	    }
	 // Test No: 022
	    // Objective:  Test GetPublication(int Cid) method returns correct  publication name which customer required from database.
	    // Objective: verify publication name in order if is required by customer. Make sure the customer with id=1 order publication 
	    // named IrishTime on orders for each day.
	    // Inputs: cid = 9 Name="Travel"
	    // Expected Output: failed 
	    // Note :The customer with id=9 doesn't exist.
	    public void testGetPublicationInvalid003() throws SQLException, MyException {
	    	
	    	leiDB = LeiDB.getInstance();
	        String pub = leiDB.getPublication(1);
	        assertFalse(pub.equalsIgnoreCase("Travel"));
	        

	    }
	    
	    

}
