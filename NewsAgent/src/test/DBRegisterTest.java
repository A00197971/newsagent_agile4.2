package test;
import java.sql.SQLException;

import main.MyException;
import main.DB;
import junit.framework.TestCase;

/**
 * 
 */

/**
 * @author Caolei
 *
 */
public class DBRegisterTest extends TestCase {

	// Test no.: 1
	// Test Objective: Test database connection
	// Input(s): none
	// Expected Outputs: connected to database successfully (boolean true)
	public void testConnection001() {
		// Create DB object
		DB db = new DB();
		// Test the method
		assertTrue(db.initialise());
	}

	// Test no:2 // Test Objective: Test database insert for register feature
	// Input(s): String firstName, String lastName, String address,String phone
	// Expected Outputs: boolean false
	// Note:The product code of DBRegister is empty yet.
	public void testInsertInValid002()throws SQLException, MyException  {
		// Create DB object
		DB db = new DB();
		// Test the method
		db.initialise();
		assertTrue(db.DBRegister("Hua", "Wu", "Jiu Jiang", "15879211276"));
	}

	// Test no:3
	// Test Objective: Test database insert for register feature
	// Input(s): String firstName, String lastName, String address,String phone
	// Expected Outputs: boolean true
	// Note:each input meets the requirement.
	public void testInsertValid003() throws SQLException, MyException {
		// Create DB object
		DB db = new DB();
		// Test the method
		db.initialise();
		assertTrue(db.DBRegister("Huaaa", "Wu", "Jiu Jiang", "15879211276"));
	}

	// Test no:4
	// Test Objective: Test database insert for register feature
	// Input(s): String firstName=null, String lastName, String address,String
	// phone
	// Expected Outputs: boolean false
	// Note:each input should not be null and the firstname is null.
	public void testInsertinValid004() throws SQLException, MyException {
		// Create DB object
		DB db = new DB();
		// Test the method
		db.initialise();
		assertFalse(db.DBRegister("", "Wu", "Jiu Jiang", "15879211276"));
	}

	// Test no:5
	// Test Objective: Test database insert for register feature
	// Input(s): String firstName=aaaaaaaaaaaaaaaaaaa(18 length), String lastName, String
	// address,String
	// phone
	// Expected Outputs: boolean false
	// Note: firstname.length is greater than 15 .
	public void testInsertInValid005()  throws SQLException, MyException{
		// Create DB object
		DB db = new DB();
		// Test the method
		db.initialise();
		assertFalse(db.DBRegister("aaaaaaaaaaaaaaaaaaa", "Wu", "Jiu Jiang",
				"15879211276"));
	}
	// Test no:6
		// Test Objective: Test database insert for register feature
		// Input(s): String firstName=Hua, String lastName, String address,String phone		
		// Expected Outputs: boolean false
		// Note: do not allow to insert same firstname and lastname,each one can only have one account.
		public void testInsertInValid006() throws SQLException, MyException {
			// Create DB object
			DB db = new DB();
			// Test the method
			db.initialise();
			assertFalse(db.DBRegister("Hua", "Wu", "Jiu Jiang",
					"15879211276"));
		}

	
			
}
