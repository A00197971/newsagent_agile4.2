package peter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import main.Customer;
import main.Order;

public final class Area {
	
	private int id;
	private int areaNumber;
	private Map<Integer, Customer> mapCustomers;
	private Map<Integer, Order> mapOrders;

	public Area(int areaNumber) {
		id = IDGenerator.getInstance().generateID();
		this.areaNumber = areaNumber;
		mapCustomers = new HashMap<>();
		mapOrders = new HashMap<>();
	}
	
	public void addNewDelivery(int orderID) {
		Order newOrder = PeterDB.getInstance().getOrder(orderID);
		Customer newCustomer = PeterDB.getInstance().getCustomer(newOrder.getcID());
		int deliveryID = IDGenerator.getInstance().generateID();
		if (newCustomer != null && newOrder != null && newCustomer.getAddress().toLowerCase().contains("athlone" + areaNumber)) {
			mapCustomers.put(deliveryID, newCustomer);
			mapOrders.put(deliveryID, newOrder);
			System.out.println("New delivery added.");
		}
	}
	
	public int getDeliveryID(Customer customer) {
		for (Integer key : mapCustomers.keySet()) {
			if (mapCustomers.get(key).equals(customer)) {
				return key;
			}
		}
		return -1;
	}
	
	public int getDeliveryID(Order order) {
		for (Integer key : mapOrders.keySet()) {
			if (mapOrders.get(key).equals(order)) {
				return key;
			}
		}
		return -1;
	}
	
	public Customer getDeliveryCustomer(int deliveryID) {
		return mapCustomers.get(deliveryID);
	}
	
	public Order getDeliveryOrder(int deliveryID) {
		return mapOrders.get(deliveryID);
	}
	
	public void calculateShortestPath() {
		//	temporary implementation by a sort by a last name
		sortCustomers();
	}
	
	public void generateDeliveryFile() {
		File deliveryFile = new File("Delivery File " + areaNumber + ".txt");
		OutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream(deliveryFile);
		} catch (Exception e) {
			System.err.println("Cannot create Delivery File.txt.");
		}
		
		//	write a header to the file
		String textLine = "ID\tFirstName\tLastName\tAddress\tPhone\tTitle\tAmount\tTotalPrice\r\n\r\n";
		byte[] data = textLine.getBytes();
		try {
			outputStream.write(data);
		} catch (Exception e) {
			System.err.println("Cannot write a header to the Delivery File.txt.");
		}
		
		//	write orders to the file
		for (Customer customer : mapCustomers.values()) {
			int idTmp = 0;
			for (Integer i : mapCustomers.keySet()) {
	            if (mapCustomers.get(i).equals(customer)) {
	                idTmp = i;
	            }
	        }
			textLine = "";
			textLine += idTmp + "\t" + customer.getFirstName() + "\t" + customer.getLastName() + "\t" + customer.getAddress() + "\t" + customer.getPhone();
			int deliveryID = getDeliveryID(customer);
			Order order = getDeliveryOrder(deliveryID);
			Product product = PeterDB.getInstance().getProduct(order.getpID());
			textLine += "\t" + product.getTitle() + "\t" + order.getAmount() + "\t" + order.getTotalPrice() + "\r\n";
			data = textLine.getBytes();
			try {
				outputStream.write(data);
			} catch (Exception e) {
				System.err.println("Cannot write a line to the Delivery File.txt.");
			}
		}
		
		//	write a delivery guy to the file
		for (Deliveryguy deliveryguy : PeterDB.getInstance().getAllDeliveryguys()) {
			if (deliveryguy.getArea().toLowerCase().contains("" + areaNumber)) {
				textLine = "\r\n" + deliveryguy.getFirstName() + "\t" + deliveryguy.getLastName() + "\r\n\r\n";
				break;
			}
		}
		data = textLine.getBytes();
		try {
			outputStream.write(data);
		} catch (Exception e) {
			System.err.println("Cannot write a header to the Delivery File.txt.");
		}
		System.out.println("File: Delivery File.txt generated.");
	}
	
	public int getAreaName(){
		return areaNumber;
	}
	
	public void setAreaName(int areaNumber) {
		this.areaNumber = areaNumber;
	}
	
	public int getID() {
		return id;
	}
	
	private void sortCustomers() {
		if (mapCustomers.size() > 1) {
			List<Customer> customersList = new ArrayList<>();
			List<Integer> customerKeysList = new ArrayList<>();
			for (Customer customer : mapCustomers.values()) {
				customersList.add(customer);
				for (Integer key : mapCustomers.keySet()) {
					if (mapCustomers.get(key).equals(customer)) {
						customerKeysList.add(key);
						break;
					}
				}
			}
			
			List<Order> ordersList = new ArrayList<>();
			List<Integer> ordersKeysList = new ArrayList<>();
			for (Order order : mapOrders.values()) {
				ordersList.add(order);
				for (Integer key : mapOrders.keySet()) {
					if (mapOrders.get(key).equals(order)) {
						ordersKeysList.add(key);
						break;
					}
				}
			}
			
			boolean doSort = false;
			do {
				doSort = false;
				for (int i = 0, j = 1; i < customersList.size() && j < customersList.size(); i++, j++) {
					Customer customer1 = customersList.get(i);
					Customer customer2 = customersList.get(j);
					Order order1 = ordersList.get(i);
					Order order2 = ordersList.get(j);
					char cust1 = customer1.getLastName().toLowerCase().charAt(0);
					char cust2 = customer2.getLastName().toLowerCase().charAt(0);
					if (Character.getNumericValue(cust2) < Character.getNumericValue(cust1)) {
						customersList.set(i, customer2);
						customersList.set(j, customer1);
						/*Integer i1 = keysList.get(i);
						Integer i2 = keysList.get(j);
						keysList.set(i, i2);
						keysList.set(j, i1);*/
						ordersList.set(i, order2);
						ordersList.set(j, order1);
						doSort = true;
					}
				}
			} while (doSort);
			
			for (int i = 0; i < customersList.size(); i++) {
				mapCustomers.replace(customerKeysList.get(i), customersList.get(i));
			}
			for (int i = 0; i < ordersList.size(); i++) {
				mapOrders.replace(ordersKeysList.get(i), ordersList.get(i));
			}
		}
	}
}
